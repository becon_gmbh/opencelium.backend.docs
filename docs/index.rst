##################
Welcome to OpenCelium REST API documentation
##################

------------

This documentation is organized into a couple of sections:

.. toctree::
   :caption: REST API Reference
   :maxdepth: 2

   authentication
   user_management
   user_group
   component
   permission
   connector
   connection
   schedule

##################
License
##################

`becon`_ © 2013-2019 becon GmbH

.. _becon: LICENSE.html
      